from src.services.model_service import feature_based_approach, finetunning_pytorch_native, finetunning_trainer_class
import src.common.settings as s


if __name__ == '__main__':
    finetunning_trainer_class(s.MODEL_NAME)

import src.common.constants as const


def _read_tsv_file(path: str) -> list:
    with open(path, 'r') as input_tsv_file:
        lines = input_tsv_file.readlines()

    lines = map(lambda line: line.strip(), lines)
    return list(map(lambda line: line.split(const.TAB), lines))


def do_format(input_path: str, output_path: str) -> None:
    splitted = _read_tsv_file(input_path)
    data = list(map(lambda line: (line[0], line[2]), splitted))
    output_lines = list(map(lambda item: item[0] + const.TAB + item[1] + const.NEW_LINE, data))

    with open(output_path, 'w') as output_tsv_file:
        output_tsv_file.writelines(output_lines)

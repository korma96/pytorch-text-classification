import src.raw_data_formatter.raw_data_formatter as formatter
import src.common.constants as const
import src.common.settings as s
from src.common.helpers import DatasetType
from transformers import PreTrainedTokenizer
import pandas as pd
from pandas import DataFrame
import os
from spellchecker import SpellChecker
import spacy
import pickle
import re
from torch.utils.data import Dataset
import torch


class CustomDataset(Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        # encodings = {'input_ids': [...], 'attention_mask': [...] }
        item = {key: torch.tensor(value[idx]) for key, value in self.encodings.items()}  # convert to torch tensor
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)


def read_data(dataset_type: DatasetType = DatasetType.TRAIN) -> DataFrame:
    if dataset_type == DatasetType.TRAIN:
        raw_data_path = s.RAW_DATA_PATH_TRAIN
        formatted_data_path = s.FORMATTED_DATA_PATH_TRAIN
    elif dataset_type == DatasetType.EVAL:
        raw_data_path = s.RAW_DATA_PATH_EVAL
        formatted_data_path = s.FORMATTED_DATA_PATH_EVAL
    else:
        raise Exception('method parameter not valid')

    if not os.path.exists(formatted_data_path):
        formatter.do_format(raw_data_path, formatted_data_path)

    return pd.read_csv(formatted_data_path, delimiter=const.TAB, names=[const.INTENTS, const.REQUESTS])


def maybe_replace(row: str, dictionary: dict) -> str:
    for key, value in dictionary.items():
        if key in row:
            row = row.replace(key, value)
    return row


def fix_misspelled_words(df: DataFrame) -> DataFrame:
    # obtained using SpellChecker package and spacy tokenization
    before_tokenization = {"a.m.": "am", "a.m": "am", "p.m.": "pm",
                           "p.m": "pm", "wed.": "wednesday", "dr.": "doctor"}
    after_tokenization = {" f ": " fahrenheit ", " c ": " celsius ", " dr ": " doctor ", " appt ": " appointment ", " fl ": " florida ",
                          " st. ": " saint ", " st ": " saint ", " nyc ": " new york city ", " nc ": " north carolina ",
                          " nj ": " new jersey ", " dc ": " district of columbia ", "celcius": "celsius", " wed ": " wednesday ",
                          "mintues": "minutes", "snoozes": "snooze", "forcast": "forecast", "tempature": "temperature",
                          "tomorrow/": "tomorrow", "temperture": "temperature", " hrs ": " hours ",
                          "apointment": "appointment", "tempurature": "temperature", "bejing": " beijing ",
                          " thurs ": " thursday ", " bday ": " birthday ", " avg ": " average ", "exerice": " exercise ",
                          "altanta": " atlanta "}
    for key, value in before_tokenization.items():                                                             # fix abbreviations
        df[const.REQUESTS] = df[const.REQUESTS].str.replace(key, value, regex=False)
    # unnecessary pycharm warning, should be ignored
    df[const.REQUESTS] = df.apply(lambda row: maybe_replace(row[const.REQUESTS], after_tokenization), axis=1)  # fix misspelled words
    return df


def preprocess(df: DataFrame, fix_misspelled=True, shuffle=True) -> DataFrame:
    df[const.REQUESTS] = list(map(lambda request: ' '.join(request.split()), df[const.REQUESTS]))  # fix whitespaces
    df[const.REQUESTS] = list(map(lambda request: request.lower(), df[const.REQUESTS]))            # lower
    to_remove = ["weekdaily"]                                                                      # remove strange words
    for word in to_remove:
        df = df[~df[const.REQUESTS].str.contains(word)]
    if fix_misspelled:
        fix_misspelled_words(df)
    df.drop_duplicates(subset=const.REQUESTS, inplace=True)                                        # drop duplicates
    if shuffle:
        df = df.sample(frac=1, random_state=s.RANDOM_STATE)                                                    # shuffle the DataFrame rows
    return df


def has_numbers(input_string):
    return bool(re.search(r'\d', input_string))


def get_spell_mistakes(file_path='files/spell_correction') -> dict:
    f = open(file_path, 'rb')
    data = pickle.load(f)
    f.close()
    return data


def show_spell_mistakes():
    data = get_spell_mistakes()
    lista = data.keys()
    lista = list(filter(lambda word: ':' not in word, lista))
    lista = list(filter(lambda word: not has_numbers(word), lista))
    #'''
    # show corrections with frequencies
    lista = sorted(lista, key=lambda word: data[word]['frequency'], reverse=True)
    for word in lista:
        print()
        print('Frequency:', data[word]['frequency'])
        print(word + ' -> ' + data[word]['correction'])
        print(data[word]['sentences'][0])
        print(data[word]['words'][0])
        print()
    #'''
    '''
    # show sentences per correction
    # fl -> florida
    # st, st. -> saint ?
    #nyc -> new york city
    #a.m -> am
    word = "appen"
    for i, sent in enumerate(data[word]['sentences']):
        print()
        print(sent)
        print(data[word]['words'][i])
        print()
    '''


def write_spell_checks_df(df: DataFrame, file_path='files/spell_correction') -> None:
    nlp = spacy.load('en_core_web_sm')
    spell = SpellChecker()
    values = df[const.REQUESTS].tolist()

    dictionary = {}
    for sentence in values:
        doc = nlp(sentence)
        words = [word.text for word in doc]
        misspelled = spell.unknown(words)
        if misspelled:
            for word in misspelled:
                if word not in dictionary:
                    dictionary[word] = {'correction': spell.correction(word), 'sentences': [], 'words': []}
                dictionary[word]['sentences'].append(sentence)
                dictionary[word]['words'].append(words)
    for word in dictionary:
        dictionary[word]['frequency'] = len(dictionary[word]['sentences'])
    outfile = open(file_path, 'wb')
    pickle.dump(dictionary, outfile)
    outfile.close()


def write_spell_checks(file_path='files/spell_correction') -> None:
    df = read_data()
    df = preprocess(df)
    write_spell_checks_df(df, file_path)


def get_intent_mapping():
    return {
        'weather/find': "what is the weather?",
        'alarm/set_alarm': "set an alarm",
        'alarm/show_alarms': "show alarms",
        'reminder/set_reminder': "set reminder",
        'alarm/modify_alarm': "modify alarm",
        'weather/checkSunrise': "check sunrise",
        'weather/checkSunset': "check sunset",
        'alarm/snooze_alarm': "snooze alarm",
        'alarm/cancel_alarm': "cancel alarm",
        'reminder/show_reminders': "show reminders",
        'reminder/cancel_reminder': "cancel reminder",
        'alarm/time_left_on_alarm': "time left on alarm"
    }


def get_intent_to_id():
    return {
        'weather/find': 0,
        'alarm/set_alarm': 1,
        'alarm/show_alarms': 2,
        'reminder/set_reminder': 3,
        'alarm/modify_alarm': 4,
        'weather/checkSunrise': 5,
        'weather/checkSunset': 6,
        'alarm/snooze_alarm': 7,
        'alarm/cancel_alarm': 8,
        'reminder/show_reminders': 9,
        'reminder/cancel_reminder': 10,
        'alarm/time_left_on_alarm': 11
    }


def get_pytorch_dataset(dataset_type: DatasetType, tokenizer: PreTrainedTokenizer, num_of_samples: int = 1000) -> Dataset:
    """
    TODO: use train_test_split instead of num_of_samples in order to keep label distribution
    """
    df = read_data(dataset_type)
    df = preprocess(df, fix_misspelled=True, shuffle=True)

    inputs = df[const.REQUESTS][:num_of_samples].tolist()
    encodings = tokenizer(inputs, truncation=True, padding=True)

    labels = df[const.INTENTS][:num_of_samples].tolist()
    intent_to_id = get_intent_to_id()
    labels = [intent_to_id[label] for label in labels]

    return CustomDataset(encodings, labels)


'''
class DataService:
    def _get_targets(self, original_data) -> list:
        targets = list(map(lambda item: item[0], original_data))
        targets = list(set(targets))
        targets.sort()
        return targets
'''

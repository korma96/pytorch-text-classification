from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from sklearn.linear_model import LogisticRegression
from torch.utils.data import DataLoader
from transformers import AutoModel, AutoTokenizer, PreTrainedTokenizer, PreTrainedModel, \
    AutoModelForSequenceClassification, TrainingArguments
from transformers import pipeline, AdamW, Trainer
from tqdm import tqdm
import torch
import src.common.settings as s
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, confusion_matrix, classification_report
from src.services.data_service import get_pytorch_dataset, read_data, preprocess, get_intent_mapping, get_intent_to_id
import src.common.constants as const
from src.common.helpers import DatasetType
import pandas as pd
from pandas import DataFrame
from src.services.visualizations_service import plot_confusion_matrix


def get_device():
    return torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')


def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, preds, average='micro')
    acc = accuracy_score(labels, preds)
    return {
        'accuracy': acc,
        'f1': f1,
        'precision': precision,
        'recall': recall
    }


def finetunning_trainer_class(model_name):
    params = {
        'num_of_samples': 100,
        'train_batch_size': 16,
        'eval_batch_size': 16,
        'learning_rate': 0.01,
        'num_of_epochs': 50
    }

    device = get_device()

    # 1) prepare data
    tokenizer: PreTrainedTokenizer = AutoTokenizer.from_pretrained(model_name)
    train_dataset = get_pytorch_dataset(DatasetType.TRAIN, tokenizer, params['num_of_samples'])
    eval_dataset = get_pytorch_dataset(DatasetType.EVAL, tokenizer, params['num_of_samples'])

    # 2) prepare for training
    model: PreTrainedModel = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=len(get_intent_to_id().keys()))
    model.to(device)
    optim = AdamW(params=model.parameters(), lr=params['learning_rate'])

    # 3) train
    training_args = TrainingArguments(
        output_dir=s.TRAINER_MODEL_SAVE_PATH,
        num_train_epochs=params['num_of_epochs'],
        per_device_train_batch_size=params['train_batch_size'],
        per_device_eval_batch_size=params['eval_batch_size'],
        warmup_steps=5,
        weight_decay=0.01,
        logging_dir=s.TRAINER_LOGS_SAVE_PATH
    )

    # default optimizer is AdamW
    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=eval_dataset,
        compute_metrics=compute_metrics
    )

    print(trainer.train())

    # 4) evaluate
    print(trainer.evaluate())

    # 5) save
    trainer.save_model(s.TRAINER_MODEL_SAVE_PATH)


def finetunning_pytorch_native(model_name):
    """
    TODO
    1) calculate accuracy
    """
    params = {
        'num_of_samples': 100,
        'train_batch_size': 16,
        'learning_rate': 5e-5,
        'num_of_epochs': 1
    }

    device = get_device()

    # 1) prepare data
    tokenizer: PreTrainedTokenizer = AutoTokenizer.from_pretrained(model_name)
    train_dataset = get_pytorch_dataset(DatasetType.TRAIN, tokenizer, params['num_of_samples'])
    eval_dataset = get_pytorch_dataset(DatasetType.EVAL, tokenizer, params['num_of_samples'])

    # 2) prepare for training
    train_loader = DataLoader(train_dataset, batch_size=params['train_batch_size'], shuffle=True)
    model: PreTrainedModel = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=len(get_intent_to_id().keys()))
    model.to(device)
    print(model.train())
    print('-------------------')
    optim = AdamW(params=model.parameters(), lr=params['learning_rate'])
    losses = []
    print(model)
    print('-------------------')
    # 3) train
    for epoch in range(params['num_of_epochs']):
        for batch in train_loader:
            optim.zero_grad()
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)
            outputs = model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)
            loss = outputs[0]
            losses.append(loss)
            loss.backward()
            optim.step()

    # 4) evaluate
    print(model.eval())

    # 5) save
    model.save_pretrained(s.PYTORCH_MODEL_SAVE_PATH)


def finetunning_freeze_base_model_params(model_name):
    """
    TODO
    """
    pass


def feature_based_approach(model_name):
    """
    TODO:
    1. Wrap model and tokenizer into pipeline
    """
    df = read_data()
    df = preprocess(df)

    # get_intent_to_id can be used
    all_intents = df[const.INTENTS].unique()
    intent_to_id = {intent: index for index, intent in enumerate(all_intents)}

    model: PreTrainedModel = AutoModel.from_pretrained(model_name)
    tokenizer: PreTrainedTokenizer = AutoTokenizer.from_pretrained(model_name)

    #print(df[const.REQUESTS][0])
    #inputs = tokenizer(df[const.REQUESTS][0])
    #print(inputs)
    num_of_samples = 1000
    x = df[const.REQUESTS][:num_of_samples].tolist()
    y = df[const.INTENTS][:num_of_samples].tolist()
    y = [intent_to_id[label] for label in y]
    y = torch.LongTensor(y)

    tokens = tokenizer(x, add_special_tokens=True, padding=True, return_tensors='pt')

    #print(tokenizer.convert_ids_to_tokens(tokens['input_ids'][0]))
    #for i in range(tokens['input_ids'].shape[0]):
    #    print("Tokens (int)      : {}".format(tokens['input_ids'][i]))
    #    print("Tokens (str)      : {}".format([tokenizer.convert_ids_to_tokens(s) for s in tokens['input_ids'][i]]))
    #    print("Tokens (attn_mask): {}".format(tokens['attention_mask'][i]))
    #    print()


    #tokenized = data.apply((lambda x: tokenizer.encode(x, add_special_tokens=True, padding=True, max_length=120, return_tensors='pt')))
    #detokenized = list(map(lambda x: tokenizer.decode(x), tokenized))
    #print(tokenizer.decode(tokenized.iloc[0]))

    #print('=========')
    with torch.no_grad():
        # same as **model
        last_hidden_states = model(input_ids=tokens['input_ids'], attention_mask=tokens['attention_mask'])
    #print(last_hidden_states)
    #print('=========')

    features = last_hidden_states[0][:, 0, :].numpy()
    labels = y

    clf = LogisticRegression()
    clf.fit(features, labels)
    print(clf.score(features, labels))


def zero_shot_test():
    # 1) prepare data
    df = read_data()
    df = preprocess(df, fix_misspelled=True)
    x = df[const.REQUESTS].tolist()
    y = df[const.INTENTS].tolist()
    x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.005, stratify=y, random_state=s.RANDOM_STATE, shuffle=True)
    # ['weather/find' 'alarm/set_alarm' 'alarm/show_alarms', 'reminder/set_reminder' 'alarm/modify_alarm' 'weather/checkSunrise', 'weather/checkSunset' 'alarm/snooze_alarm' 'alarm/cancel_alarm', 'reminder/show_reminders' 'reminder/cancel_reminder', 'alarm/time_l
    #candidate_labels = ["what is the weather?", "set an alarm", "show alarms", "set reminder", "modify alarm",
                        #"check sunrise", "check sunset", "snooze alarm", "cancel alarm", "show reminders", "cancel reminder", "time left on alarm"]
    intent_mapping = get_intent_mapping()
    '''
    counter = 0
    length = len(x_train)
    batches = []
    inc = 10
    while counter < length:
        batches.append(x_train[counter:counter+inc])
        counter += inc
    #print(len(batches))
    #print(length)
    '''
    outputs = []
    # 2) classify
    hypothesis_template = "{}"
    targets = list(intent_mapping.values())
    task = "zero-shot-classification"
    classifier = pipeline(task)
    #for batch in tqdm(batches):
        #output = classifier(batch, candidate_labels, hypothesis_template=hypothesis_template)
    for i in tqdm(x_train):
        output = classifier(i, targets, hypothesis_template=hypothesis_template)
        #print(output['sequence'], output['labels'][0], output['scores'][0])
        outputs.append(output['sequence'] + '\t' +
                       output['labels'][0] + '\t' + output['labels'][1] + '\t' + output['labels'][2] + '\t' +
                       str(output['scores'][0]) + '\t' + str(output['scores'][1]) + '\t' + str(output['scores'][2]) + '\n')

    # 3) save outputs
    with open('files/output.tsv', 'w') as tsv_file:
        tsv_file.writelines(outputs)


def zero_shot_results():
    df: DataFrame = pd.read_csv('files/output.tsv', delimiter=const.TAB, names=[const.REQUESTS,
        const.INTENTS + '1', const.INTENTS + '2', const.INTENTS + '3', 'score1', 'score2', 'score3'])
    print(df[const.INTENTS + '1'].value_counts())

    x = df[const.REQUESTS].values.tolist()
    y = df[const.INTENTS + '1'].values.tolist()

    intent_mapping = get_intent_mapping()
    df = read_data()
    df = preprocess(df, fix_misspelled=True)
    x_dest = df[const.REQUESTS].tolist()
    y_dest = df[const.INTENTS].tolist()
    y_true = []
    for xx in x:
        index = x_dest.index(xx)
        dest = y_dest[index]
        y_true.append(intent_mapping[dest])

    f1 = f1_score(y_true, y, average='micro')
    acc = accuracy_score(y_true, y)
    print('f1 score', round(f1, 2))
    print('accuracy', round(acc, 2))
    print(classification_report(y_true, y))
    cm = confusion_matrix(y_true, y, labels=list(intent_mapping.values()))
    plot_confusion_matrix(cm, classes=list(intent_mapping.values()))


'''
class CustomNN(nn.Module):

    def __init__(self):
        super().__init__()

    def forward(self, x):
        pass
'''

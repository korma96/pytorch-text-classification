import src.common.constants as const
from enum import Enum


class DatasetType(Enum):
    TRAIN = 0,
    EVAL = 1,
    TEST = 2


def normalize_string(string: str):
    # 1) lowercase
    string = string.lower()
    # 2) replace numbers
    for num in const.NUMBERS:
        if num in string:
            string = string.replace(num, const.NUMERIC)
    # 3) remove unnecessary whitespaces
    return ' '.join(string.split())

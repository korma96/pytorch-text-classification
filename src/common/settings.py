
RAW_DATA_PATH_TRAIN = './dataset/01_raw_data/train-en.tsv'
FORMATTED_DATA_PATH_TRAIN = './dataset/02_formatted_data/train-en.tsv'
RAW_DATA_PATH_EVAL = './dataset/01_raw_data/eval-en.tsv'
FORMATTED_DATA_PATH_EVAL = './dataset/02_formatted_data/eval-en.tsv'
PYTORCH_MODEL_SAVE_PATH = './output/models/finetunning/distillbert'
TRAINER_MODEL_SAVE_PATH = './output/models/trainer/distillbert'
TRAINER_LOGS_SAVE_PATH = './output/logs/trainer/distillbert'

MODEL_NAME = "distilbert-base-uncased"
RANDOM_STATE = 42
